# base image
FROM tiangolo/uwsgi-nginx-flask:python3.7-alpine3.7

ARG git_version
ENV APP_VERSION=$git_version

COPY ./requirements.txt /var/www/requirements.txt

# install dependencies
RUN apk add --no-cache --virtual=.build_dependencies musl-dev gcc python3-dev libffi-dev linux-headers && \
    apk add postgresql-dev libpq make && \
    pip install -r /var/www/requirements.txt && \
    rm -rf ~/.cache/pip && \
    apk del .build_dependencies

WORKDIR /var/www/app
COPY . /var/www/app
