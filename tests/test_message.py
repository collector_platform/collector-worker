"""Test messages"""
import asyncio
from app.services.message import send_sms, send_mail
from app.services.message import send_report_mail
from app.utils.file import make_csv

def test_sms_output():
    # data = {
    #     'receiver':'256777342233',
    #     'sender':'8008',
    #     'message':'Test Message'
    # }
    # result = send_sms(data)
    # assert result == data['message']
    pass

def test_email_output():
    """test email"""
    data = {
        'email':'test@gmail.com',
        'sender':'8008',
        'message':'Test Message',
        'subject': 'Invoice reminder',
        'balance': '50000',
        'name': 'Allan Kasibante',
        'entity': 'Service Company',
        'uuid': 'CFR0002'
    }
    result = asyncio.run(send_mail(data))
    assert result == data['email']

def test_send_report():
    """test report generator"""
    entity = {
        'email': 'test@gmail.com'
    }
    line = ['test', 'csv']
    filename = make_csv("./reports/test.csv", line)

    result = asyncio.run(send_report_mail(entity, filename))
    assert isinstance(result, str)
