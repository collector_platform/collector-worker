from app.services.check import health

def test_check_output(capsys):
    n = 8
    health(n)
    captured = capsys.readouterr()
    assert captured.out == "Check {0}\n".format(n)
