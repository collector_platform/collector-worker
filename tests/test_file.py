"""Test file methods"""
import asyncio
from app.services.file import generate_report

def test_generate_report():
    """test report generator"""
    result = asyncio.run(generate_report('bb87ac8659ad41108da0c81d7f0d95ab'))
    assert isinstance(result, str)
