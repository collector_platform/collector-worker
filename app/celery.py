"""Celery initialisation"""
from celery import Celery
from celery.schedules import crontab

app = Celery('app.tasks')
app.config_from_object('app.config')

# add tasks to the beat schedule
app.conf.beat_schedule = {
    "health-task": {
        "task": "app.tasks.health_check",
        "schedule": crontab(minute="0,30")
    },
    "reminder-task": {
        "task": "app.tasks.send_reminders",
        "schedule": crontab(day_of_month=1, hour=7,)
    },
    "reports-task": {
        "task": "app.tasks.send_reports",
        "schedule": crontab(day_of_month=1, hour=7,)
    }
}
