"""App Tasks"""
import asyncio
from app.celery import app
from app.services.check import health
from app.services.message import send_sms, send_mail, send_report_mail
from app.services.file import generate_report, get_entities
from app.utils.dbaccess import query


@app.task
def health_check() -> None:
    """Task to send birth wishes"""
    health("health")


@app.task
def send_reminders():
    """Task to send reminders for unpaid
       invoices to customers
    """

    sql = """ c.uuid, c.name, c.phone, c.email, b.balance,
    e.name as entity from accounts a inner join customer c on c.uuid=a.owner_id
    inner join account_balances b on a.id=b.id
    inner join entity e on c.entity_id=e.uuid
    where b.balance<0
    """

    results = query(sql)

    for result in results:
        result['subject'] = result.get('entity') + ' Payment Reminder'
        asyncio.run(send_sms(result))
        asyncio.run(send_mail(result))


@app.task
def send_reports():
    """Send monthly reports to collectors"""
    for entity in get_entities():
        file_path = asyncio.run(generate_report(entity))
        asyncio.run(send_report_mail(entity, file_path))
