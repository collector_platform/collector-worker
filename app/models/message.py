
import json
import base64
from abc import ABC, abstractmethod

from sendgrid import SendGridAPIClient, Attachment
from sendgrid.helpers.mail import Mail

from requests import post
from app import settings
class MessageInterface(ABC):
    """Message Interface"""

    def __init__(self, **kwargs):
        self.receipient = kwargs['receipient']
        self.sender = kwargs['sender']
        self.message = kwargs['message']

    @abstractmethod
    def send(self):
        """abstract method"""
        pass


class Sms(MessageInterface):
    """SMS"""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
    async def send(self):
        """send a message"""
        try:
            data = {
                'receiver': self.receipient,
                'body': self.message,
                'type': 'reminder'
            }

            rep = post(
                settings.API_URL + 'api/v1/sms',
                data=json.dumps(data),
                headers=self.sms_headers()
            )
            print(rep)
            if rep.status_code == 401:
                rep = self.send()

            result = json.loads(rep.content.decode('utf-8'))
            print(result)
            return self.message
        except ConnectionError:
            return self.message
    
    def sms_headers(self):
        """api headers"""
        auth_token = settings.AUTH_TOKEN
    
        if auth_token != '':
            return {
            'content-type': 'application/json',
            'authorization': 'Bearer %s' % settings.AUTH_TOKEN
        }

        data = {
            'username': settings.AUTH_KEY,
            'password': settings.AUTH_PASS
        }

        rep = post(
            settings.API_URL + 'auth/login',
            data=json.dumps(data),
            headers={'content-type': 'application/json'}
        )

        result = json.loads(rep.content.decode('utf-8'))

        try:
            settings.AUTH_TOKEN = result['access_token']
        except KeyError:
            settings.AUTH_TOKEN = ''

        return {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % settings.AUTH_TOKEN
        }


class Email(MessageInterface):
    """Email"""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.subject = kwargs['subject']
        self.filename = kwargs['filename'] if 'filename' in kwargs else None

    async def send(self):
        message = Mail(
            from_email='soloozacompany@gmail.com',
            to_emails=self.receipient,
            subject=self.subject,
            plain_text_content=self.message)

        if self.filename:
            attachment = self.build_attachment(self.filename)
            message.add_attachment(attachment)
        try:
            client = SendGridAPIClient(settings.SENDGRID_API_KEY)
            response = client.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(str(e))
        return self.receipient

    def build_attachment(self, file_path):
        """Build attachment"""
        with open(file_path, 'rb') as filename:
            data = filename.read()

        # Encode contents of file as Base 64
        encoded = base64.b64encode(data).decode()
        print(encoded)
        attachment = Attachment()
        attachment.content = encoded
        attachment.filename = "report.csv"
        return attachment
