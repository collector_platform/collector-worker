"""All file manipulation methods"""
import json
from requests import get
from ..utils.dbaccess import query
from ..utils.file import make_csv
from .. import settings
from ..utils.helpers import headers

async def generate_report(entity):
    """Generate a report for an entity"""
    rep = get(
        settings.API_URL + 'api/v1/customers?entity=' + entity['uuid'],
        headers=headers()
    )

    if rep.status_code == 401:
        rep = generate_report(entity)

    result = json.loads(rep.content.decode('utf-8'))
    print(result)
    customers = result['results']

    headings = ['Reference', 'Name', 'Phone', 'Subscription', 'Balance']
    lines = [headings]

    for customer in customers:
        line = [
            customer.get('uuid'),
            customer.get('name'),
            customer.get('phone'),
            customer['subscription']['category'],
            customer['account']['balance']
        ]
        # line = [customer[key] for key in customer.keys()]
        lines.append(line)
    file_path = "./reports/" + entity.get('name') + ".csv"
    return make_csv(file_path, lines)


def get_entities():
    """Get all entitties"""
    sql = """ * FROM entity"""
    return query(sql)
