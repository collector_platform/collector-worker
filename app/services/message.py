"""messaging service"""
from app.models.message import Sms, Email
from app.utils.mail import pymail

async def send_sms(data):
    """send sms"""
    amount = "{:,}".format(float(data['balance']))
    msg = """Hello {0}, please clear your debt of UGX {1}""".format(
        data['name'],
        amount,
    ) + """ at {0}. Reference: {1}""".format(
        data['entity'],
        data['uuid']
    )

    sms = Sms(
        receipient=data['phone'],
        sender='8080',
        message=msg
    )
    return await sms.send()

async def send_mail(data):
    """send mail"""
    amount = "{:,}".format(float(data['balance']))
    msg = """Hello {0}, please clear your debt of UGX {1}""".format(
        data['name'],
        amount,
    ) + """ at {0}. Reference: {1}""".format(
        data['entity'],
        data['uuid']
    )

    email = Email(
        receipient=data['email'],
        sender='8080',
        message=msg,
        subject=data['subject']
    )
    return await email.send()

async def send_report_mail(entity, filepath):
    """send mail"""
    msg = """Hello, please find your monthly collections report attached"""

    email = Email(
        receipient=entity['email'],
        sender='8080',
        message=msg,
        subject='Monthly Collections Report',
        filename=filepath
    )
    return await pymail(email)
