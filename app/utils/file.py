"""File utils"""
import os
import csv

def make_csv(filename, lines):
    """Create a csv"""
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        for line in lines:
            print(line)
            writer.writerow(line)
    return filename
