import psycopg2
import sqlalchemy
from app import settings


def query(sql):
    """Query database"""
    conn = psycopg2.connect(settings.DATABASE_URL)
    cursor = conn.cursor()
    cursor.execute("select json_agg(t) from (SELECT " + sql + ")  t")
    rows = cursor.fetchall()
    conn.close()
    return rows[0][0]


def execute_sql(file):
    """Execute sql query"""
    # file = open(file_path)
    engine = sqlalchemy.create_engine(settings.DATABASE_URL)
    escaped_sql = sqlalchemy.text(file.read())
    engine.execute(escaped_sql)
