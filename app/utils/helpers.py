"""Helper utilities"""
import json
from requests import post
from .. import settings

def headers():
    """api headers"""
    auth_token = settings.AUTH_TOKEN
    if auth_token != '':
        return {
            'content-type': 'application/json',
            'authorization': 'Bearer %s' % settings.AUTH_TOKEN
        }

    data = {
        'username': settings.AUTH_KEY,
        'password': settings.AUTH_PASS
    }

    rep = post(
        settings.API_URL + 'auth/login',
        data=json.dumps(data),
        headers={'content-type': 'application/json'}
    )

    result = json.loads(rep.content.decode('utf-8'))

    try:
        settings.AUTH_TOKEN = result['access_token']
    except KeyError:
        settings.AUTH_TOKEN = ''

    return {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer %s' % settings.AUTH_TOKEN
    }
