"""Celery configuration"""
from . import settings

broker_url = settings.REDIS_URL
result_backend = 'rpc://'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
# timezone = 'Europe/Oslo'
enable_utc = True

imports = ["app.tasks"]
