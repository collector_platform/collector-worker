"""Default configuration

Use env var to override
"""
from environs import Env

env = Env()
env.read_env()

API_URL = env.str('API_URL', default='http://localhost:5000/')
AUTH_TOKEN = ''
AUTH_KEY = env.str('AUTH_KEY', default='AUTH_KEY')
AUTH_PASS = env.str('AUTH_PASS', default='AUTH_PASS')
DATABASE_URL = env.str('DATABASE_URL', default='postgres://')
REDIS_URL = env.str('REDIS_URL', default='redis://')
SENDGRID_API_KEY = env.str('SENDGRID_API_KEY', default='SENDGRID_API_KEY')
MEDIA_ROOT = '/'

MAIL_SERVER = env.str('MAIL_SERVER', default='MAIL_SERVER')
MAIL_PORT = env.str('MAIL_PORT', default='MAIL_PORT')
MAIL_USE_TLS = env.str('MAIL_USE_TLS', default='MAIL_USE_TLS')
MAIL_USE_SSL = env.str('MAIL_USE_SSL', default='MAIL_USE_SSL')
MAIL_USERNAME = env.str('MAIL_USERNAME', default='MAIL_USERNAME')
MAIL_PASSWORD = env.str('MAIL_PASSWORD', default='MAIL_PASSWORD')

DAY = env.str('DAY', default="2")
HOUR = env.str('HOUR', default="17")
