.PHONY: clean system-packages python-packages install tests run all

clean:
	find . -type f -name '*.pyc' -delete
	find . -type f -name '*.log' -delete

system-packages:
	sudo apt install python-pip -y

pip:
	pip install -r requirements.txt

install: system-packages pip

lint:
	flake8 --max-line-length=120 app/*.py
	pylint -d C0301 app/*.py

tests:
	tox

beat:
	celery -A app beat -l INFO

worker:
	celery -A app worker -l INFO

all: clean lint tests beat worker
