"""Setup config"""
from setuptools import setup, find_packages

__version__ = '0.1'


setup(
    name='collector-worker',
    version=__version__,
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'celery',
        'redis',
        'python-dotenv'
    ],
)
